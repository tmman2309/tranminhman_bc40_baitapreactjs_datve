import "./App.css";
import Ex_Datve_Redux from "./Ex_Datve_Redux/Ex_Datve_Redux";

function App() {
  return (
    <div
      style={{
        background: "url(./datve_Image/bgmovie.jpg)",
        backgroundRepeat: "no-repeat",
        backgroundPosition: "center",
        backgroundSize: "cover",
        backgroundAttachment: "fixed",
        minHeight: "100vh",
      }}
    >
      <Ex_Datve_Redux />
    </div>
  );
}

export default App;
