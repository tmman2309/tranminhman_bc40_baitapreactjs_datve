import React, { Component } from 'react'
import { connect } from 'react-redux'
import Select_Seat_Item from './Select_Seat_Item'

class Select_Seat_List extends Component {
  render() {
    return (
      <>
        {this.props.danhSachGhe.map((item) => {
          return(
            <Select_Seat_Item key={item.hang} item={item} />
          )
        })}
      </>
    )
  }
}

let mapStateToProps = (state) => {
  return{
    danhSachGhe: state.datveReducer.danhSachGhe,
  }
}

export default connect(mapStateToProps)(Select_Seat_List)