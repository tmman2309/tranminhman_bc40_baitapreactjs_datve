import React, { Component } from "react";
import Fill_Form from "./Fill_Form";
import Select_Seat from "./Select_Seat";

export default class Ex_Datve_Redux extends Component {
  render() {
    return (
      <div className="container">
        <h1 className="text-center text-white font-weight-bold display-4 p-5">
          MOVIE SEAT SELECTION
        </h1>
        <div className="content d-flex justify-content-center">
          <div
            style={{ width: "700px", background: "rgba(128, 128, 128, 0.34)" }}
            className="p-5"
          >
            <Fill_Form />
            <Select_Seat />
          </div>
        </div>
        <p className="text-center text-white font-weight-bold p-5">
          © 2023 Movie Seat Selection . All Rights Reserved | Design by ManTran
        </p>
      </div>
    );
  }
}
