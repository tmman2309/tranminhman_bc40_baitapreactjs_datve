import React, { Component } from "react";
import { connect } from "react-redux";
import styles from "./Ex_Datve.module.css";
import { confirmSelection } from "./redux/action/datveAction";
import Select_Seat_List from "./Select_Seat_List";

class Select_Seat extends Component {
  render() {
    let {username, seatNumber, seats} = this.props;

    return (
      <div>
        {/* -------------seat info------------ */}
        <div className="seat-info d-flex text-white p-3">
          <p className={styles.greenbox}>Selected Seat</p>
          <p className={styles.redbox}>Reserved Seat</p>
          <p className={styles.whitebox}>Empty Seat</p>
        </div>
        {/* -------------select seat------------ */}
        <div className="select-seat">
          {/* title */}
          <div id="titleSelectSeat" className="d-none justify-content-center">
            <p style={{ maxWidth: "max-content" }} className="bg-warning">
              Please Select your Seats NOW!
            </p>
          </div>
          {/* select area */}
          <div className="d-flex justify-content-center">
            <table className="text-white font-weight-bold">
              <tbody id="gheList">
                <Select_Seat_List />
              </tbody>
            </table>
          </div>
          {/* screen */}
          <p
            style={{
              fontSize: "20px",
              letterSpacing: "7px",
              wordSpacing: "10px",
            }}
            className="bg-warning text-center py-3 my-4 font-weight-bold"
          >
            SCREEN THIS WAY
          </p>
          {/* confirm button */}
          <div className="d-flex justify-content-center pb-4">
            <button onClick={() => {
              this.props.handleConfirmSelection(username,seatNumber,seats)
            }} className="btn btn-light">Confirm Selection</button>
          </div>
        </div>
        {/* -------------confirm seat------------ */}
        <div className="confirm-seat container">
          <table
            style={{ tableLayout: "fixed", width: "100%", maxWidth: "500px", border: "2px solid black" }}
            className="bg-white table-bordered"
          >
            <thead className="text-center">
              <tr>
                <th
                  style={{ width: "36%", border: "2px solid black"}}
                  className="py-2"
                >
                  Name
                </th>
                <th
                  style={{ width: "32%", border: "2px solid black"}}
                  className="py-2"
                >
                  Number of Seats
                </th>
                <th
                  style={{ width: "32%", border: "2px solid black"}}
                  className="py-2"
                >
                  Seats
                </th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td
                  id="usernameConfirm"
                  style={{ width: "36%", border: "2px solid black", overflowWrap: "break-word", wordWrap: "break-word"}}
                  className="px-2 py-4"
                ></td>
                <td
                  id="seatNumberConfirm"
                  style={{ width: "32%", border: "2px solid black", overflowWrap: "break-word", wordWrap: "break-word"}}
                  className="px-2 py-4"
                ></td>
                <td
                  id="seatsConfirm"
                  style={{ width: "32%", border: "2px solid black", overflowWrap: "break-word", wordWrap: "break-word"}}
                  className="px-2 py-4"
                ></td>
              </tr>
            </tbody>
          </table>
        </div>

        <div id="alertSuccess" className="font-italic text-success mt-4"></div>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return{
    username: state.datveReducer.username,
    seatNumber: state.datveReducer.seatNumber,
    seats: state.datveReducer.seats,
  }
}

let mapDispatchToProps = (dispatch) => {
  return{
    handleConfirmSelection: (username,seatNumber,seats) => {
      dispatch(confirmSelection(username,seatNumber,seats))
    }
  }
}

export default connect(mapStateToProps,mapDispatchToProps)(Select_Seat)
