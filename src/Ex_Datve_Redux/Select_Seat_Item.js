import React, { Component } from 'react'
import Select_Seat_Item_Mini from './Select_Seat_Item_Mini'

export default class Select_Seat_Item extends Component {

  // autorun nên không đưa vào reducerRedux được
  handleKhoangCachHangNgang = () => {
    if(this.props.item.hang == "E"){
      return (
        <div className='mb-5'>E</div>
      )
    } else{
      return this.props.item.hang;
    }
  }

  render() {
    return (
      <>
        <tr>
          <td>{this.handleKhoangCachHangNgang()}</td>
          {this.props.item.danhSachGhe.map((item) => {
            return <Select_Seat_Item_Mini key={item.soGhe} item={item}/>
          })}
        </tr>
      </>
    )
  }
}