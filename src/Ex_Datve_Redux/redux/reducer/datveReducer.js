import {
  HANDLE_CONFIRM_SELECTION,
  HANDLE_SEAT_NUMBER,
  HANDLE_START_SELECT,
} from "../constants/datveConstants";

let seatsListJsonOld = localStorage.getItem("seats");
let seatsList = [];

if (seatsListJsonOld != null) {
  seatsList = JSON.parse(seatsListJsonOld);
}

let initialValue = {
  danhSachGhe: [
    {
      hang: "",
      danhSachGhe: [
        { soGhe: "1", gia: 0 },
        { soGhe: "2", gia: 0 },
        { soGhe: "3", gia: 0 },
        { soGhe: "4", gia: 0 },
        { soGhe: "5", gia: 0 },
        { soGhe: "6", gia: 0 },
        { soGhe: "7", gia: 0 },
        { soGhe: "8", gia: 0 },
        { soGhe: "9", gia: 0 },
        { soGhe: "10", gia: 0 },
        { soGhe: "11", gia: 0 },
        { soGhe: "12", gia: 0 },
      ],
    },
    {
      hang: "A",
      danhSachGhe: [
        { soGhe: "A1", gia: 75000, daDat: false },
        { soGhe: "A2", gia: 75000, daDat: false },
        { soGhe: "A3", gia: 75000, daDat: false },
        { soGhe: "A4", gia: 75000, daDat: false },
        { soGhe: "A5", gia: 75000, daDat: false },
        { soGhe: "A6", gia: 75000, daDat: false },
        { soGhe: "A7", gia: 75000, daDat: false },
        { soGhe: "A8", gia: 75000, daDat: false },
        { soGhe: "A9", gia: 75000, daDat: false },
        { soGhe: "A10", gia: 75000, daDat: false },
        { soGhe: "A11", gia: 0, daDat: true },
        { soGhe: "A12", gia: 0, daDat: true },
      ],
    },
    {
      hang: "B",
      danhSachGhe: [
        { soGhe: "B1", gia: 75000, daDat: false },
        { soGhe: "B2", gia: 75000, daDat: false },
        { soGhe: "B3", gia: 75000, daDat: false },
        { soGhe: "B4", gia: 75000, daDat: false },
        { soGhe: "B5", gia: 75000, daDat: false },
        { soGhe: "B6", gia: 75000, daDat: false },
        { soGhe: "B7", gia: 75000, daDat: false },
        { soGhe: "B8", gia: 75000, daDat: false },
        { soGhe: "B9", gia: 75000, daDat: false },
        { soGhe: "B10", gia: 75000, daDat: false },
        { soGhe: "B11", gia: 75000, daDat: false },
        { soGhe: "B12", gia: 75000, daDat: false },
      ],
    },
    {
      hang: "C",
      danhSachGhe: [
        { soGhe: "C1", gia: 75000, daDat: false },
        { soGhe: "C2", gia: 75000, daDat: false },
        { soGhe: "C3", gia: 75000, daDat: false },
        { soGhe: "C4", gia: 75000, daDat: false },
        { soGhe: "C5", gia: 75000, daDat: false },
        { soGhe: "C6", gia: 75000, daDat: false },
        { soGhe: "C7", gia: 75000, daDat: false },
        { soGhe: "C8", gia: 75000, daDat: false },
        { soGhe: "C9", gia: 75000, daDat: false },
        { soGhe: "C10", gia: 75000, daDat: false },
        { soGhe: "C11", gia: 75000, daDat: false },
        { soGhe: "C12", gia: 75000, daDat: false },
      ],
    },
    {
      hang: "D",
      danhSachGhe: [
        { soGhe: "D1", gia: 75000, daDat: false },
        { soGhe: "D2", gia: 75000, daDat: false },
        { soGhe: "D3", gia: 75000, daDat: false },
        { soGhe: "D4", gia: 75000, daDat: false },
        { soGhe: "D5", gia: 75000, daDat: false },
        { soGhe: "D6", gia: 75000, daDat: false },
        { soGhe: "D7", gia: 75000, daDat: false },
        { soGhe: "D8", gia: 75000, daDat: false },
        { soGhe: "D9", gia: 75000, daDat: false },
        { soGhe: "D10", gia: 75000, daDat: false },
        { soGhe: "D11", gia: 75000, daDat: false },
        { soGhe: "D12", gia: 75000, daDat: false },
      ],
    },
    {
      hang: "E",
      danhSachGhe: [
        { soGhe: "E1", gia: 75000, daDat: false },
        { soGhe: "E2", gia: 75000, daDat: false },
        { soGhe: "E3", gia: 75000, daDat: false },
        { soGhe: "E4", gia: 75000, daDat: false },
        { soGhe: "E5", gia: 75000, daDat: false },
        { soGhe: "E6", gia: 75000, daDat: false },
        { soGhe: "E7", gia: 75000, daDat: false },
        { soGhe: "E8", gia: 75000, daDat: false },
        { soGhe: "E9", gia: 75000, daDat: false },
        { soGhe: "E10", gia: 75000, daDat: false },
        { soGhe: "E11", gia: 75000, daDat: false },
        { soGhe: "E12", gia: 75000, daDat: false },
      ],
    },
    {
      hang: "F",
      danhSachGhe: [
        { soGhe: "F1", gia: 75000, daDat: false },
        { soGhe: "F2", gia: 75000, daDat: false },
        { soGhe: "F3", gia: 75000, daDat: false },
        { soGhe: "F4", gia: 75000, daDat: false },
        { soGhe: "F5", gia: 75000, daDat: false },
        { soGhe: "F6", gia: 75000, daDat: false },
        { soGhe: "F7", gia: 75000, daDat: false },
        { soGhe: "F8", gia: 75000, daDat: false },
        { soGhe: "F9", gia: 75000, daDat: false },
        { soGhe: "F10", gia: 75000, daDat: false },
        { soGhe: "F11", gia: 75000, daDat: false },
        { soGhe: "F12", gia: 75000, daDat: false },
      ],
    },
    {
      hang: "G",
      danhSachGhe: [
        { soGhe: "G1", gia: 75000, daDat: false },
        { soGhe: "G2", gia: 75000, daDat: false },
        { soGhe: "G3", gia: 75000, daDat: false },
        { soGhe: "G4", gia: 75000, daDat: false },
        { soGhe: "G5", gia: 75000, daDat: false },
        { soGhe: "G6", gia: 75000, daDat: false },
        { soGhe: "G7", gia: 75000, daDat: false },
        { soGhe: "G8", gia: 75000, daDat: false },
        { soGhe: "G9", gia: 75000, daDat: false },
        { soGhe: "G10", gia: 75000, daDat: false },
        { soGhe: "G11", gia: 75000, daDat: false },
        { soGhe: "G12", gia: 75000, daDat: false },
      ],
    },
    {
      hang: "H",
      danhSachGhe: [
        { soGhe: "H1", gia: 75000, daDat: false },
        { soGhe: "H2", gia: 75000, daDat: false },
        { soGhe: "H3", gia: 75000, daDat: false },
        { soGhe: "H4", gia: 75000, daDat: false },
        { soGhe: "H5", gia: 75000, daDat: false },
        { soGhe: "H6", gia: 75000, daDat: false },
        { soGhe: "H7", gia: 75000, daDat: false },
        { soGhe: "H8", gia: 75000, daDat: false },
        { soGhe: "H9", gia: 75000, daDat: false },
        { soGhe: "H10", gia: 75000, daDat: false },
        { soGhe: "H11", gia: 75000, daDat: false },
        { soGhe: "H12", gia: 75000, daDat: false },
      ],
    },
    {
      hang: "I",
      danhSachGhe: [
        { soGhe: "I1", gia: 75000, daDat: false },
        { soGhe: "I2", gia: 75000, daDat: false },
        { soGhe: "I3", gia: 75000, daDat: false },
        { soGhe: "I4", gia: 75000, daDat: false },
        { soGhe: "I5", gia: 75000, daDat: false },
        { soGhe: "I6", gia: 75000, daDat: false },
        { soGhe: "I7", gia: 75000, daDat: false },
        { soGhe: "I8", gia: 75000, daDat: false },
        { soGhe: "I9", gia: 75000, daDat: false },
        { soGhe: "I10", gia: 75000, daDat: false },
        { soGhe: "I11", gia: 75000, daDat: false },
        { soGhe: "I12", gia: 75000, daDat: false },
      ],
    },
    {
      hang: "J",
      danhSachGhe: [
        { soGhe: "J1", gia: 75000, daDat: false },
        { soGhe: "J2", gia: 75000, daDat: false },
        { soGhe: "J3", gia: 75000, daDat: false },
        { soGhe: "J4", gia: 75000, daDat: false },
        { soGhe: "J5", gia: 75000, daDat: false },
        { soGhe: "J6", gia: 75000, daDat: false },
        { soGhe: "J7", gia: 75000, daDat: false },
        { soGhe: "J8", gia: 75000, daDat: false },
        { soGhe: "J9", gia: 75000, daDat: false },
        { soGhe: "J10", gia: 75000, daDat: false },
        { soGhe: "J11", gia: 75000, daDat: false },
        { soGhe: "J12", gia: 75000, daDat: false },
      ],
    },
  ],
  username: "",
  seatNumber: null,
  seats: [],
  seatsList: seatsList, //from localStorage
};

export const datveReducer = (state = initialValue, action) => {
  switch (action.type) {
    // ------------Fill_Form------------
    // Fill_Form: HANDLE_START_SELECT
    case HANDLE_START_SELECT: {
      let seatsOrdered = state.seatsList.length;
      let seatsTotal =
        state.danhSachGhe[0].danhSachGhe.length *
        (state.danhSachGhe.length - 1);
      let seatsLeft = seatsTotal - seatsOrdered;

      if (action.payload.username.length != 0 && action.payload.number > 0) {
        if (action.payload.number <= seatsLeft) {
          // show title
          document.getElementById("titleSelectSeat").classList.remove("d-none");
          document.getElementById("titleSelectSeat").classList.add("d-flex");

          // disable input
          document.getElementById("username").setAttribute("disabled", "");
          document.getElementById("quantity").setAttribute("disabled", "");

          // enable select
          let tableID = document.getElementById("gheList");
          let gheList = tableID.getElementsByTagName("input");

          for (let i = 0; i < gheList.length; i++) {
            let item = gheList[i];
            item.removeAttribute("disabled");
          }

          for (let i = 0; i < gheList.length; i++) {
            let item = gheList[i];

            if (state.seatsList.length != 0) {
              state.seatsList.forEach((element) => {
                if (item.id == element) {
                  item.setAttribute("disabled", "");
                }
              });
            }
          }
        } else {
          alert("Số ghế bạn điền đã vượt quá số ghế trống hiện tại!");
        }
      } else {
        alert("Vui lòng điền thông tin hợp lệ!");
      }

      return {
        ...state,
        username: action.payload.username,
        seatNumber: action.payload.number,
      };
    }

    // ------------Select_Seat------------
    // Select_Seat_Item: HANDLE_SEAT_NUMBER
    case HANDLE_SEAT_NUMBER: {
      if (action.payload.id != null) {
        let seats = [];

        document.getElementById(action.payload.id).toggleAttribute("checked");

        let tableID = document.getElementById("gheList");
        let gheList = tableID.getElementsByTagName("input");
        let checkedList = tableID.querySelectorAll("[checked]");

        if (checkedList.length >= action.payload.number) {
          for (let i = 0; i < gheList.length; i++) {
            let item = gheList[i];
            item.setAttribute("disabled", "");
          }

          for (let i = 0; i < gheList.length; i++) {
            let item = gheList[i];
            for (let index = 0; index < checkedList.length; index++) {
              let itemChecked = checkedList[index];

              if (item.id == itemChecked.id) {
                seats.push(item.id);
                item.removeAttribute("disabled");
              }
            }
          }
        } else {
          for (let i = 0; i < gheList.length; i++) {
            let item = gheList[i];
            item.removeAttribute("disabled");
          }

          for (let i = 0; i < gheList.length; i++) {
            let item = gheList[i];

            if (state.seatsList.length != 0) {
              state.seatsList.forEach((element) => {
                if (item.id == element) {
                  item.setAttribute("disabled", "");
                }
              });
            }
          }
        }

        return { ...state, seats: seats };
      }
    }
    // Select_Seat: HANDLE_CONFIRM_SELECTION
    case HANDLE_CONFIRM_SELECTION: {
      let { username, seatNumber, seats } = action.payload;

      if (seats.length == seatNumber) {
        let seatsListJsonOld = localStorage.getItem("seats");
        let seatsList = [];

        if (seatsListJsonOld != null) {
          seatsList = JSON.parse(seatsListJsonOld);

          seats.forEach((item) => {
            seatsList.push(item);
          });
        } else {
          seatsList = [...seats];
        }

        document.getElementById(
          "usernameConfirm"
        ).innerHTML = `<p>${username}</p>`;
        document.getElementById(
          "seatNumberConfirm"
        ).innerHTML = `<p>${seatNumber}</p>`;
        document.getElementById(
          "seatsConfirm"
        ).innerHTML = `<p>${seats.toString()}</p>`;
        document.getElementById("alertSuccess").innerHTML =
          "**Cảm ơn bạn đã đặt vé! Chúc bạn có giây phút thư giãn tuyệt vời!";

        let seatsListJsonNew = JSON.stringify(seatsList);
        localStorage.setItem("seats", seatsListJsonNew);
      } else if (seats.length > seatNumber) {
        alert(
          "Số ghế bạn chọn đã vượt quá số ghế bạn điền! Vui lòng refresh và chọn lại!"
        );
      }
    }

    default:
      return state;
  }
};
