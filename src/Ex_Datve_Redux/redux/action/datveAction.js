import {
  HANDLE_CONFIRM_SELECTION,
  HANDLE_SEAT_NUMBER,
  HANDLE_START_SELECT,
} from "../constants/datveConstants";

export const startSelectAction = (username, number) => ({
  type: HANDLE_START_SELECT,
  payload: { username, number },
});

export const seatNumberAction = (id,number) => ({
  type: HANDLE_SEAT_NUMBER,
  payload: {id,number},
});

export const confirmSelection = (username, seatNumber, seats) => ({
  type: HANDLE_CONFIRM_SELECTION,
  payload: {username, seatNumber, seats}
})

