import React, { Component } from "react";
import { connect } from "react-redux";
import { seatNumberAction } from "./redux/action/datveAction";
import styles from "./Ex_Datve.module.css";

class Select_Seat_Item_Mini extends Component {
  // autorun nên không đưa vào reducerRedux được
  handleKhoangCachHangDocMini = () => {
    let isNumber = true;

    // Kiểm tra soGhe là Number hay là String
    if (isNaN(this.props.item.soGhe * 1)) {
      isNumber = false;
    } else {
      isNumber = true;
    }

    // Nếu number thì hiển thị number, nếu string thì hiển thị ô input
    // Kiểm tra ghế đã được đặt hay chưa, nếu rồi thì tô đỏ và disabled
    let {seatsList} = this.props;

    if (isNumber) {
      if (this.props.item.soGhe * 1 == 5) {
        return <div className="ml-4 mr-5">5</div>;
      } else {
        return <div className="ml-4">{this.props.item.soGhe}</div>;
      }
    } else {
      if (this.props.item.soGhe == "E5") {
        let chooseStyles = styles.ghe;

        if (seatsList.length != 0) {
          seatsList.forEach((item) => {
            if(item == "E5"){
              chooseStyles = styles.gheDaDat;
            }
          });
        }

        return (
          <div className="form-check mb-5 mr-5">
            <input
              onChange={() => {
                this.props.handleSeatNumber(
                  this.props.item.soGhe,
                  this.props.seatNumber
                );
              }}
              id="E5"
              className={chooseStyles}
              type="checkbox"
              defaultValue
              disabled
            />
          </div>
        );
      } else if (this.props.item.soGhe.endsWith("5")) {

        let chooseStyles = styles.ghe;

        if (seatsList.length != 0) {
          seatsList.forEach((item) => {
            if(item == this.props.item.soGhe){
              chooseStyles = styles.gheDaDat;
            }
          });
        }

        return (
          <div className="form-check mr-5">
            <input
              onChange={() => {
                this.props.handleSeatNumber(
                  this.props.item.soGhe,
                  this.props.seatNumber
                );
              }}
              id={this.props.item.soGhe}
              className={chooseStyles}
              type="checkbox"
              defaultValue
              disabled
            />
          </div>
        );
      } else if (this.props.item.soGhe.startsWith("E")) {

        let chooseStyles = styles.ghe;

        if (seatsList.length != 0) {
          seatsList.forEach((item) => {
            if(item == this.props.item.soGhe){
              chooseStyles = styles.gheDaDat;
            }
          });
        }

        return (
          <div className="form-check mb-5">
            <input
              onChange={() => {
                this.props.handleSeatNumber(
                  this.props.item.soGhe,
                  this.props.seatNumber
                );
              }}
              id={this.props.item.soGhe}
              className={chooseStyles}
              type="checkbox"
              defaultValue
              disabled
            />
          </div>
        );
      } else {

        let chooseStyles = styles.ghe;

        if (seatsList.length != 0) {
          seatsList.forEach((item) => {
            if(item == this.props.item.soGhe){
              chooseStyles = styles.gheDaDat;
            }
          });
        }

        return (
          <div className="form-check">
            <input
              onChange={() => {
                this.props.handleSeatNumber(
                  this.props.item.soGhe,
                  this.props.seatNumber
                );
              }}
              id={this.props.item.soGhe}
              className={chooseStyles}
              type="checkbox"
              defaultValue
              disabled
            />
          </div>
        );
      }
    }
  };

  render() {
    return (
      <>
        <td
          style={{ width: "40px", height: "24px" }}
          className="text-center ml-5"
        >
          {this.handleKhoangCachHangDocMini()}
        </td>
      </>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    seatNumber: state.datveReducer.seatNumber,
    seatsList: state.datveReducer.seatsList,
  };
};

let mapDispatchToProps = (dispatch) => {
  return {
    handleSeatNumber: (id, number) => {
      dispatch(seatNumberAction(id, number));
    },
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Select_Seat_Item_Mini);
