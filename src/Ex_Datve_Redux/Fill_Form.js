import React, { Component } from "react";
import { connect } from "react-redux";
import { startSelectAction } from "./redux/action/datveAction";

class Fill_Form extends Component {
  state = {
    username: "",
    quantity: "",
  }

  handleChangeInput = (event) => {
    let {value, name} = event.target;
    this.setState({
      [name]: value,
    })
  }

  render() {

    return (
      <div>
        <p className="text-warning pb-3">
          Fill The Required Details Below And Select Your Seats
        </p>
        <div className="d-flex">
          <div className="form-group mr-5 w-50">
            <label className="text-white font-weight-bold">
              Name <span className="text-danger">*</span>
            </label>
            <input
              onChange={this.handleChangeInput}
              type="text"
              className="form-control"
              value={this.state.username}
              name="username"
              id="username"
            />
          </div>
          <div className="form-group">
            <label className="text-white font-weight-bold">
              Quantity <span className="text-danger">*</span>
            </label>
            <input
              onChange={this.handleChangeInput}
              type="number"
              className="form-control"
              value={this.state.quantity}
              name="quantity"
              id="quantity"
            />
          </div>
        </div>
        <button
          onClick={() => {
            this.props.handleStartSelect(this.state.username,this.state.quantity)
          }}
          style={{ borderRadius: "10px" }}
          className="btn btn-light my-3 px-4 font-weight-bold"
        >
          Start Selecting
        </button>
      </div>
    );
  }
}

let mapDispatchToProps = (dispatch) => {
  return {
    handleStartSelect: (username, number) => {
      dispatch(startSelectAction(username,number));
    },
  };
};

export default connect(null,mapDispatchToProps)(Fill_Form)
